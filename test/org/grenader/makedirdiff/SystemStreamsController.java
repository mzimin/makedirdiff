/*********************************************************************
 *                                                                   *
 *  Make Directory Difference ( MakeDirDiff )                        *
 *  Author: Igor Kanshyn                                             *
 *  Copyright (c) 2009, Igor Kanshyn. All rights reserved.           *
 *                                                                   *
 *  MakeDirDiff is released under the Apache Software License.       *
 *  See LICENSE.txt for more details.                                *
 *                                                                   *
 *********************************************************************/
package org.grenader.makedirdiff;

import java.io.PrintStream;
import java.io.ByteArrayOutputStream;
import java.util.regex.Pattern;

/**
 * <p>Product: MakeDirDiff ( Make Directory Difference )<br> Author: Igor Kanshyn (grenader). </p>
 * Date: Sep 11, 2009<br>
 *
 * <p> Test Helper class. <code>SystemStreamsController</code> redefines
 * system error and output streams and gives access to their content.<br>
 * </p>
 *
 * <p>How to use:</p>
 * <blockquote><pre>
 * // create and redefine stream
 * SystemStreamsController streamsController = new SystemStreamsController();
 *
 * // Execute some code that writes into System.err and/or System.out
 * // ... your code here ...
 *
 * // get streams content into String vars
 * String err = streamsController.getErrStream();
 * String out = streamsController.getOutStream();
 *
 * // restore the original state (system err and out stream)
 * streamsController.restoreSystemStreams();
 * </pre></blockquote>
 */
public class SystemStreamsController {
  private PrintStream originalErr;
  private PrintStream originalOut;

  private ByteArrayOutputStream errStream;
  private ByteArrayOutputStream outStream;

  /**
   * Creates a <code>SystemStreamsController</code> instance.<br>
   * Saves system default out and err streams and attach system streams to a local streams stored in the <code>SystemStreamsController</code> instance.
   * <br><br>
   *  Use {@link #getErrStream getErrStream}() and {@link #getOutStream getOutStream}() to get stream content<br>
   *  Use {@link #restoreSystemStreams restoreSystemStreams}() to restore the orignal system streams
   */
  public SystemStreamsController() {
    saveSystemStreams();
    redefineStream();
  }

  private void redefineStream() {
    errStream = new ByteArrayOutputStream(1024);
    System.setErr(new PrintStream(errStream));

    outStream = new ByteArrayOutputStream(1024);
    System.setOut(new PrintStream(outStream));
  }

  private void saveSystemStreams() {
    originalErr = System.err;
    originalOut = System.out;
  }

  /**
   * Restore system default err and out streams.
   */
  public void restoreSystemStreams() {
    System.setErr(originalErr);
    System.setOut(originalOut);
  }

  /**
   *
   * @return error streat content
   */
  public String getErrStream() {
    return errStream.toString();
  }

  /**
   *
   * @return output stream content
   */
  public String getOutStream() {
    return outStream.toString();
  }
}
