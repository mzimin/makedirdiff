Used for deploying to Salesforce only changed files. - MZ

---

Make Directory Difference  ( MakeDirDiff )
Version 1.0 (18th September 2009)


Copyright 2009, Igor Kanshyn (igor.kanshyn@gmail.com)
http://code.google.com/p/makedirdiff/


Introduction
============
MakeDirDiff compares two directories and creates the third directory with the 
first directory structure, but with new or updated files only. It can update 
the second directory content with the current content of the first directory. 
On the next run MakeDirDiff will find and place only new/changed files appeared
in the first directory into the third directory 
Look at the MakeDirDiff.gif picture to get a visual notion about MakeDirDiff 
data flow.

How it can be run
=================
It is a simple java application, designed as an ANT (http://ant.apache.org/) task.
MakeDirDiff can be 
- run from the command line, 
- run as a single Ant task, 
- used as a part of other Ant tasks or 
- integrated into other applications like a library as well.


System Requirements
===================
JRE or JDK 1.6 or higher.
MakeDirDiff is a java application, so it can be run on every operation system 
where Java works. You only need Java installed. Go to the 
http://java.sun.com/javase/downloads/index.jsp and download JRE/JDK for your OS
if you do not have java installed yet.

Apache ANT 1.6 or higher.
Since version 1.0 MakeDirDiff works as an ANT task. If you would like to run it 
using an ANT script, you will need Apache Ant on your system. You can always 
download a fresh version of Apache Ant using the 
http://ant.apache.org/bindownload.cgi page.


Running 'Make Directory Difference' tool
=================================

1. Running as an ANT task.
--------------------------
1. Download makeDirDiff-bin-xx.zip file from the 
   http://code.google.com/p/makedirdiff/downloads/list page.
2. Extract this archive to a directory.
3. If you wish more details, look into 'build.xml' file to get some information
   about a way the example Ant task is defined there, otherwise skip this step.
4. Go to this directory and run an 'ant' command using a system command line 
   interface.
5. You will see some output and words 'BUILD SUCCESSFUL' at the end. In this 
   case the provided MakeDirDiff example works fine. This example task creates
   a 'result' directory and copy there all files from 'original' directory. 
   It also stores an 'original' directory content into the 'previous' directory.
5.1. Something has gone wrong when you don't see 'BUILD SUCCESSFUL' words. 
     Look at the 'Troubleshooting' chapter here to find out what is your problem.
6. That's it! MakeDirDiff made a copy of new/updates files from the 'original' 
   directory for you.

7. Let's try to make something more difficult. Please, remove the 'result' 
   directory now and run 'ant' command again. You will see that the 'result' 
   directory is created again, but it's empty. This is because it has only 
   new/updated files and directories from the 'original' directory, which have 
   not been changed.
8. Make a small experiment then. Add some files and directories into the 
   'original' directory and run the 'ant' command again. The 'result' directory 
   will contain all those new files and directories. It works!

2. Running as a java application 
--------------------------
TBD ...


Features
========

1. As mentioned above, MakeDirDiff compares two directories and creates the 
   third directory with the first directory structure, but will put new or 
   updated files there only. MakeDirDiff is looking for an 'old' copy of data 
   in the 'previous' directory.

2. MakeDirDiff is an Ant task. It can be used independently or in your build, 
   backup or update Ant scripts.

3. MakeDirDiff is a command line tool. It doesn't have an interface. It doesn't 
   bother you with questions and windows.

4. MakeDirDiff uses Ant FileSet (http://ant.apache.org/manual/CoreTypes/fileset.html) 
   element. 
   Using this element you can:
     a. define your 'original' directory as a set of different real directories,
     b. make MakeDirDiff process only some subdirectories from the original file set,
     c. include only some specific files into your processing;
     d. skip some files and/or directories from processing;
   Please, look at the http://ant.apache.org/manual/CoreTypes/fileset.html page for 
   more information.

5. By default MakeDirDiff copies all 'original' directory data into the 'previous' 
   directory. Your can turn off this feature using 'keepOldFiles' task attribute 
   or java property.

6. Cleaning the 'result' directory.
   To be added ...


Troubleshooting
===============
1. Check whether you have java installed.
  - run 'java -version' in your system command line interface. 
  You have to see an output with a version of your Java VM.
  Note that MakeDirDiff requires java 1.6 and higher.
2. You have java on your system installed, but 'java -version' still doesn't show 
  you an error message.
  - you need to add your jdk1.6.0_<version>\bin directory to the PATH variable. 
  Look at the http://www.java.com/en/download/help/path.xml instruction to do so.
3. Check whether you have Apache Ant installed.
  - run 'ant -version' in your system command line interface. 
  You have to see an output with version of an Ant tool installed on your system.
  Note that MakeDirDiff requires Apache Ant 1.6 and higher.
4. You have Apache Ant on your system installed, but 'ant -version' still shows 
  you an error message.
  - you need to add your apache-ant-1.x.<version>\bin directory to the PATH 
  variable. Look at the http://www.java.com/en/download/help/path.xml 
  instruction to do so.

  
License
=======

MakeDirDiff is released under the Apache Software License. See LICENSE.txt for 
more details.


How to contribute
=================
The program has an open source license and the source code is available at 
http://code.google.com/p/makedirdiff/source/checkout page.

If you want to contribute you may become a member of the project which gives 
you a write access to the subversion server.

If you just find a bug or you feel that MakeDirDiff doesn't have a significant 
feature, please, raise a bug on http://code.google.com/p/makedirdiff/issues/list 
or write me to igor.kanshyn@gmail.com

I will be happy to get any feedback.


Acknowledgements
================

I'd like to thank Dmitry Solomadin who was so kind and smart to invent a name 
of the MakeDirDiff tool for me.
